//
//  HomeViewController.swift
//  Hemita_test
//
//  Created by apple on 31/08/21.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK:- Outlet Connections
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var clnView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!

    //MARK:- Variables
    var arrImages = [UIImage(named: "demoImage3"),
                     UIImage(named: "demoImage3"),
                     UIImage(named: "demoImage3")
    ]
    var counter = 0
    var timer = Timer()
    var data = ["Raspberry", "Apple", "Guaua", "Pineapple",
        "Custurd apple", "Watermelon", "Banana", "Cherry",
        "Pear", "Orange", "Sweet lime", "Lemon",
        "Jackfruit", "Litchi", "Kiwi", "Cranberry",
        "Bluberry", "Grapes", "Mango", "Melon"]
    
    var filteredData: [String]!
    var searchResults : [String] = []
    let searchController = UISearchController(searchResultsController: nil)
    var index = 0
    var arrData = [TableViewDetailModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        tblView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailTableViewCell")
        tblView.register(UINib(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderTableViewCell")
        
        clnView.delegate = self
        clnView.dataSource = self
        clnView.isPagingEnabled = true
        clnView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        
        pageControl.currentPageIndicatorTintColor = UIColor.blue
        pageControl.pageIndicatorTintColor = UIColor.black
        pageControl.numberOfPages = arrImages.count
        pageControl.currentPage = 0
//        DispatchQueue.main.async {
//            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.scrollImage), userInfo: nil, repeats: true)
//        }
        
        filteredData = data
        self.tblView.keyboardDismissMode = .onDrag
        
        searchController.searchResultsUpdater = self
        self.definesPresentationContext = true
        
    }
    
    
    
    @objc func scrollImage(){
        if counter < arrImages.count {
            let index = IndexPath.init(item: counter, section: 0)
            self.clnView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageControl.currentPage = counter
            counter += 1
            self.index = counter
            pageControl.currentPageIndicatorTintColor = UIColor.blue
            
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.clnView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageControl.currentPage = counter
            counter = 1
            self.index = counter
            pageControl.currentPageIndicatorTintColor = UIColor.blue
        }
        
        filteredData =  data.shuffled()
        
        self.tblView.reloadData()
        
    }
    
    @objc func getSwipeAction( _ recognizer : UISwipeGestureRecognizer){

        if recognizer.direction == .right{
           print("Right Swiped")
        } else if recognizer.direction == .left {
            print("Left Swiped")
        }
    }

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.filteredData.count > 0 {
            return self.filteredData.count
        }else{
            return self.data.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell: DetailTableViewCell = tblView.dequeueReusableCell(withIdentifier: "DetailTableViewCell", for: indexPath) as! DetailTableViewCell
        if self.filteredData.count > 0{
            cell.lblText.text = self.filteredData[indexPath.row]
        }else{
            cell.lblText.text = self.data[indexPath.row]
        }
        
            return cell

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            let headerCell = tblView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
        
            headerCell.searchBar.delegate = self

            headerView.addSubview(headerCell)
            return headerView

        }

        func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
            return 70
        }
}


//MARK:- CollectionView delegate and datasource images
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
          
        var swipeGesture  = UITapGestureRecognizer()
        swipeGesture = UITapGestureRecognizer(target: self, action: #selector(self.scrollImage))
        cell.imgView.isUserInteractionEnabled = true
        cell.imgView.addGestureRecognizer(swipeGesture)
        
        cell.imgView.image = arrImages[indexPath.row]
//                if let vc = cell.viewWithTag(1) as? UIImageView {
//                    vc.image = arrImages[indexPath.row]
//                }

        return cell
    }
    
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = clnView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}

extension HomeViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredData = searchText.isEmpty ? data : data.filter({(dataString: String) -> Bool in
            
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })

        self.tblView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension HomeViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {

            if let searchText = searchController.searchBar.text {
                filteredData = data.filter({ $0.contains(searchText) })
                
                self.tblView.reloadData()
            }
        }
}


extension Array
{
    /** Randomizes the order of an array's elements. */
    mutating func shuffle()
    {
        for _ in 0..<3
        {
            sort { (_,_) in arc4random() < arc4random() }
        }
    }
}
